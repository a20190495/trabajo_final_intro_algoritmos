#include "upc.h"
#include "Menu.h" 
#include "Mapas.h" 

#include <iostream>
#include <stdio.h>
#include <conio.h>
using namespace std;

//funiones implementadas hasta el momento

void dibujar_personaje(double x, double y, int n, int v);
void borrar(double x, double y, int n);
bool updateHero(int& row, int& col, char key = 0);
void updateEnemy(int& row, int& col, int& dir);
bool checkCollision(int heroRow, int heroCol, int enemyRow, int enemyCol);


int main() {
    int initialHeroRow = 1, initialHeroCol = 0;
    int row = initialHeroRow, col = initialHeroCol;
    int erow = 5, ecol = 1;
    int edir = 1;
    bool gameover = false;
    char key;

    hideCursor();
    //showMaze();
    updateHero(row, col, key);

    while (!gameover) {
        if (_kbhit()) {
            char key = toupper(_getch());
            switch (key) {
            case 27: // ESC
                gameover = true;
                break;
            case 'W':
            case 'A':
            case 'S':
            case 'D':
                if (updateHero(row, col, key)) {
                    gameover = true;
                }
            }
        }
        if (checkCollision(row, col, erow, ecol)) {
            row = initialHeroRow;
            col = initialHeroCol;
            updateHero(row, col);
        }
        updateEnemy(erow, ecol, edir);
        sleep4(100);
    }

    gotoxy(0, MAXROWS + 1);
    system("pause");
    resetAll();

    return EXIT_SUCCESS;
}

void dibujar_personaje(double x, double y, int n, int v) {

    printxy(x, y, " o ");
    printxy(x, y + 1, "-l-");
    printxy(x, y + 2, " M ");
}


void borrar(double x, double y, int n) {
    if (n == 1) {
        printxy(x, y, " ");
        printxy(x, y + 1, " ");
        printxy(x, y + 2, " ");
    }
    if (n == 2) {
        printxy(x, y, " ");
        printxy(x, y + 1, " ");
        printxy(x, y + 2, " ");

}

bool updateHero(int& row, int& col, char key = 0) {
    int nrow = row, ncol = col;
    switch (key) {
    case 'W': --nrow; break;
    case 'A': --ncol; break;
    case 'S': ++nrow; break;
    case 'D': ++ncol; break;
    }
    //if (nrow < 0 || nrow >= MAXROWS || ncol < 0 || ncol >= MAXCOLS
    //    || maze[nrow][ncol] != 1) {
    //    return false;
    //}
    gotoxy(col, row);
    cout << ' ';
    row = nrow;
    col = ncol;
    gotoxy(col, row);
    foreground(DARK_CYAN);
    cout << HERO;
    if (row == MAXROWS - 2 && col == MAXCOLS - 1) {
        gotoxy(0, MAXROWS);
        cout << "You won\n";
        return true;
    }
    return false;
}

void updateEnemy(int& row, int& col, int& dir) {
    gotoxy(col, row);
    cout << ' ';
    //if (maze[row][col + dir] == 0) {
    //    dir = -dir;
    //}
    col += dir;
    gotoxy(col, row);
    foreground(BRIGHT_RED);
    cout << ENEMY;
}

bool checkCollision(int heroRow, int heroCol, int enemyRow, int enemyCol) {
    return heroRow == enemyRow && heroCol == enemyCol;
}


int logicGame(){
//movimiento del personaje y logica cuando termine el mapa
}