#ifndef MENU_H
#define MENU_H


#include <string>
#include "upc.h"
#include <random>

using namespace std;

using uint = unsigned int;
const uint MAX_COLUMNS = 80;
const uint MAX_ROWS = 21;
const uint width = 65;
const uint height = 19;

const string TL = "\xc9";
const string TR = "\xbb";
const string BL = "\xc8";
const string BR = "\xbc";
const string HO = "\xcd";
const string VE = "\xba";

void logicMenu();
int menuPrincipal();
void opcion1();
void opcion2();
void opcion3();
void printFrame();
void printxy(uint x, uint y, string txt);

//opciones del menu

void logicMenu() {

    char x;

    do {
        x = menuPrincipal();
        switch (x) {
        case 'p':
        case 'P':
            opcion1();
            break;
        case 'i':
        case 'I':
            opcion2();
            break;
        case 'c':
        case 'C':
            opcion3();
            break;
        }
    } while (x != 'e' && x != 'E');

}

// Aun se vera si sera solo ANSII o la impresion de un mapa con el 
// mismo metodo para implementar el mapa

int menuPrincipal() {

    uint leftPos = MAX_COLUMNS / 2 - width / 2;
    uint topPos = MAX_ROWS / 2 - height / 2;


    char op;

    do {
        clear();
        printFrame();

        printxy(leftPos + 15, topPos + 5, "");
        printxy(leftPos + 15, topPos + 6, "");
        printxy(leftPos + 15, topPos + 7, "");
        printxy(leftPos + 15, topPos + 8, "");
        printxy(leftPos + 12, topPos + 1, "");
        printxy(leftPos + 12, topPos + 2, "");
        printxy(leftPos + 12, topPos + 3, "");
        printxy(leftPos + 12, topPos + 4, "");

        printxy(leftPos + 14, topPos + 10, "");
        printxy(leftPos + 18, topPos + 11, "");
        printxy(leftPos +  2, topPos + 17, "");
        printxy(leftPos + 53, topPos + 17, "");
        printxy(leftPos + 33, topPos + 14, "");
        printxy(leftPos + 31, topPos + 14, "");
        cin >> op;

    } while (op != 'p' && op != 'e' && op != 'P' && op != 'E' && op != 'c' && op != 'C' && op != 'i' && op != 'I');

    return op;
}

// El juego tal cual

void opcion1() {

    clear();
    //printFrame(leftPos, topPos, width, height);
    //logicGame();
    _getch();
}

// Explicacion del juego mas intrucciones basicas
// de matemática

void opcion2() {

    uint leftPos = MAX_COLUMNS / 2 - width / 2;
    uint topPos = MAX_ROWS / 2 - height / 2;
    clear();
    printFrame();
    printxy(leftPos + 11, topPos + 2, "");
    printxy(leftPos + 11, topPos + 3, "");
    printxy(leftPos + 11, topPos + 4, "");

    printxy(leftPos +  7, topPos + 6, "");
    printxy(leftPos +  6, topPos + 7, "");
    printxy(leftPos +  6, topPos + 8, "");
    printxy(leftPos +  9, topPos + 9, "");
    printxy(leftPos + 11, topPos + 11, "");
    printxy(leftPos + 15, topPos + 12, "");
    printxy(leftPos + 15, topPos + 13, "");
    printxy(leftPos +  7, topPos + 14, "");
    printxy(leftPos + 18, topPos + 15, "");
    printxy(leftPos + 23, topPos + 16, "");
    printxy(leftPos + 62, topPos + 17, "");
    _getch();
}

// Creditos

void opcion3() {

    uint leftPos = MAX_COLUMNS / 2 - width / 2;
    uint topPos = MAX_ROWS / 2 - height / 2;
    clear();
    printFrame();
    printxy(leftPos + 17, topPos + 2, "");
    printxy(leftPos + 17, topPos + 3, "");
    printxy(leftPos + 17, topPos + 4, "");
    printxy(leftPos + 17, topPos + 5, "");
    printxy(leftPos + 19, topPos + 6, "");
    printxy(leftPos + 19, topPos + 7, "");
    printxy(leftPos + 18, topPos + 8, "");
    printxy(leftPos + 19, topPos + 9, "");

    printxy(leftPos + 28, topPos + 12, "MEMBERS:");
    printxy(leftPos + 16, topPos + 13, "+ Macedo Macavilca, Alvaro Adrian");
    printxy(leftPos + 16, topPos + 14, "+ Sandoval Aiquipa, Kelber Yamir");
    printxy(leftPos + 16, topPos + 15, "+ Huaman Cuba, Johan Giovani");
    printxy(leftPos + 62, topPos + 17, " ");
    _getch();
}

//Probablemente en este frame se encaje una introduccion tipo 
//mapa, es decir, con colores y que todo el cmd este pintado del
//color que escojamos

void printFrame() {

    uint leftPos = MAX_COLUMNS / 2 - width / 2;
    uint topPos = MAX_ROWS / 2 - height / 2;
    printxy(leftPos, topPos, TL);
    printxy(leftPos + width - 1, topPos, TR);
    printxy(leftPos, topPos + height - 1, BL);
    printxy(leftPos + width - 1, topPos + height - 1, BR);

    for (int i = 1; i < width - 1; ++i) {
        printxy(leftPos + i, topPos, HO);
        printxy(leftPos + i, topPos + height - 1, HO);
    }
    for (int i = 1; i < height - 1; ++i) {
        printxy(leftPos, topPos + i, VE);
        printxy(leftPos + width - 1, topPos + i, VE);
    }
}


void printxy(uint x, uint y, string txt) {
    gotoxy(x, y);
    cout << txt;
}

#endif